package com.turddles.models;

public class Tenant {
	private String tenantName;
	private String tenantAddress;
	private String tenantEmail;
	private String tenantPhone;
	private String tenantStatus;
	private String username;
	private String password;
	
	public Tenant(String tenantName, String tenantAddress, String tenantEmail, String tenantPhone, String tenantStatus,
			String username, String password) {
		this.tenantName = tenantName;
		this.tenantAddress = tenantAddress;
		this.tenantEmail = tenantEmail;
		this.tenantPhone = tenantPhone;
		this.tenantStatus = tenantStatus;
		this.username = username;
		this.password = password;
	}

	public String getTenantName() {
		return tenantName;
	}
	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}
	public String getTenantAddress() {
		return tenantAddress;
	}
	public void setTenantAddress(String tenantAddress) {
		this.tenantAddress = tenantAddress;
	}
	public String getTenantEmail() {
		return tenantEmail;
	}
	public void setTenantEmail(String tenantEmail) {
		this.tenantEmail = tenantEmail;
	}
	public String getTenantPhone() {
		return tenantPhone;
	}
	public void setTenantPhone(String tenantPhone) {
		this.tenantPhone = tenantPhone;
	}
	public String getTenantStatus() {
		return tenantStatus;
	}
	public void setTenantStatus(String tenantStatus) {
		this.tenantStatus = tenantStatus;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	
}
