package com.turddles.models;

public class Guardian {
	private String name;
	private String address;
	private String email;
	private String phone;
	private String alternatePhone;
	private String guardianType;
	
	public Guardian(String name, String address, String email, String phone, String alternatePhone,
			String guardianType) {
		
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.alternatePhone = alternatePhone;
		this.guardianType = guardianType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAlternatePhone() {
		return alternatePhone;
	}
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}
	public String getGuardianType() {
		return guardianType;
	}
	public void setGuardianType(String guardianType) {
		this.guardianType = guardianType;
	}
}
