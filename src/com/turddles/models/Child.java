package com.turddles.models;

import java.sql.Date;


public class Child {
	//private int id;
//	private int classroomId;
	//private int tenantId;
	private String firstName;
	private String lastName;
	private Date birthday;
	private Date enrollmentDate;
	private Date departureDate;
	private String enrollmentStatus;
	private String schedule;
	private String history;
	private String notes;
	
	public Child(){}
	
	public Child(
			String firstName, 
			String lastName, 
			Date birthday, 
			Date enrollmentDate, 
			Date departureDate, 
			String enrollmentStatus,
			String schedule,
			String notes, 
			String history){
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
		this.enrollmentDate = enrollmentDate;
		this.departureDate = departureDate;
		this.enrollmentStatus = enrollmentStatus;
		this.schedule = schedule;
		this.notes = notes;
		this.history = history;
	}
	
	/*public int getClassroomId() {
		return classroomId;
	}

	public void setClassroomId(int classroomId) {
		this.classroomId = classroomId;
	}

	public int getTenantId() {
		return tenantId;
	}

	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}*/

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public Date getEnrollmentDate() {
		return enrollmentDate;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public String getSchedule() {
		return schedule;
	}
	
	public String getEnrollmentStatus(String enrollmentStatus) {
		return enrollmentStatus;
	}
	
	public String getHistory() {
		return history;
	}

	public void setHistory(String history) {
		this.history = history;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public void setEnrollmentDate(Date enrollmentDate) {
		this.enrollmentDate = enrollmentDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	
	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
}
