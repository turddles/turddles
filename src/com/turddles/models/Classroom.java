package com.turddles.models;

public class Classroom {

	private String name;
	private int start_age;
	private int end_age;
	private int size;

	public Classroom(String name, int start_age, int end_age, int size) {
		
		this.name = name;
		this.start_age = start_age;
		this.end_age = end_age;
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStart_age() {
		return start_age;
	}

	public void setStart_age(int start_age) {
		this.start_age = start_age;
	}

	public int getEnd_age() {
		return end_age;
	}

	public void setEnd_age(int end_age) {
		this.end_age = end_age;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + end_age;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + size;
		result = prime * result + start_age;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classroom other = (Classroom) obj;
		if (end_age != other.end_age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (size != other.size)
			return false;
		if (start_age != other.start_age)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Classroom [name=" + name + ", start_age=" + start_age + ", end_age=" + end_age + ", size=" + size + "]";
	}
}
