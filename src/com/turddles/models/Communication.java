package com.turddles.models;

public class Communication {

	private String recepient;
	private String text;
	private String communicationType;
	
	public Communication(String recepient, String text, String communicationType) {
		
		this.recepient = recepient;
		this.text = text;
		this.communicationType = communicationType;
	}

	public String getRecepient() {
		return recepient;
	}

	public void setRecepient(String recepient) {
		this.recepient = recepient;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCommunicationType() {
		return communicationType;
	}

	public void setCommunicationType(String communicationType) {
		this.communicationType = communicationType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((communicationType == null) ? 0 : communicationType.hashCode());
		result = prime * result + ((recepient == null) ? 0 : recepient.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Communication other = (Communication) obj;
		if (communicationType == null) {
			if (other.communicationType != null)
				return false;
		} else if (!communicationType.equals(other.communicationType))
			return false;
		if (recepient == null) {
			if (other.recepient != null)
				return false;
		} else if (!recepient.equals(other.recepient))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Communication [recepient=" + recepient + ", text=" + text + ", communicationType=" + communicationType
				+ "]";
	}
}
