package com.turddles.models;

public class Staff {
	private String firstname;
	private String lastname;
	private String address;
	private String email;
	private String phone;
	private String alternatePhone;
	private String staffRole;
	private String assignedClass;
	
	public Staff(String firstname, String lastname, String address, String email, String phone, String alternatePhone,
			String staffRole, String assignedClass) {
		
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.alternatePhone = alternatePhone;
		this.staffRole = staffRole;
		this.assignedClass = assignedClass;
	}
	
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAlternatePhone() {
		return alternatePhone;
	}
	public void setAlternatePhone(String alternatePhone) {
		this.alternatePhone = alternatePhone;
	}
	public String getStaffRole() {
		return staffRole;
	}
	public void setStaffRole(String staffRole) {
		this.staffRole = staffRole;
	}
	public String getAssignedClass() {
		return assignedClass;
	}
	public void setAssignedClass(String assignedClass) {
		this.assignedClass = assignedClass;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((alternatePhone == null) ? 0 : alternatePhone.hashCode());
		result = prime * result + ((assignedClass == null) ? 0 : assignedClass.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((staffRole == null) ? 0 : staffRole.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Staff other = (Staff) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (alternatePhone == null) {
			if (other.alternatePhone != null)
				return false;
		} else if (!alternatePhone.equals(other.alternatePhone))
			return false;
		if (assignedClass == null) {
			if (other.assignedClass != null)
				return false;
		} else if (!assignedClass.equals(other.assignedClass))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (staffRole == null) {
			if (other.staffRole != null)
				return false;
		} else if (!staffRole.equals(other.staffRole))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Staff [firstname=" + firstname + ", lastname=" + lastname + ", address=" + address + ", email=" + email
				+ ", phone=" + phone + ", alternatePhone=" + alternatePhone + ", staffRole=" + staffRole
				+ ", assignedClass=" + assignedClass + "]";
	}
}
