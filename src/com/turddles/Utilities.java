package com.turddles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.core.SecurityContext;

import com.turddles.dao.implementations.TenantDaoImpl;
import java.security.Principal;

public final class Utilities {

	public static Connection connectToDB(){
		Connection connect = null;
		String host = "localhost:3306";
		String database = "turddles";
		String user = "root";
		String password = "admin";
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/turddles?"
			          + "user=root&password=admin");
					//("jdbc:mysql://" + host + "/" + database + "/" , user, password);
		}
		catch(SQLException | ClassNotFoundException e){
			// log exception
		}
		return connect;
	}	
	
	public static void closeConnection(ResultSet rs, PreparedStatement stmt, Connection connect){
		try {
			if(rs != null){
				rs.close();
			}
			if(stmt != null){
				stmt.close();
			}
			if(connect != null){
				connect.close();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static int getCurrentUserTenantId(SecurityContext context){
		TenantDaoImpl tenant = new TenantDaoImpl();
		Principal principal = context.getUserPrincipal();
		String username = principal.getName();
		
		return tenant.getTenantId(username);	
	}
}
