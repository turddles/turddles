package com.turddles;

	import javax.ws.rs.ext.ContextResolver;
	import javax.ws.rs.ext.Provider;

	import org.codehaus.jackson.map.AnnotationIntrospector;
	import org.codehaus.jackson.map.AnnotationIntrospector.Pair;
	import org.codehaus.jackson.map.DeserializationConfig;
	import org.codehaus.jackson.map.ObjectMapper;
	import org.codehaus.jackson.map.SerializationConfig;
	import org.codehaus.jackson.map.SerializationConfig.Feature;
	import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
	import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.MapperFeature;

	
	@Provider
	public class MyObjectMapperProvider implements ContextResolver<ObjectMapper> {

		private static final ObjectMapper MAPPER = new ObjectMapper();
	    
	    static {
	   //   MAPPER.setSerializationInclusion(Include.NON_EMPTY);
	  //    MAPPER.disable(MapperFeature.USE_GETTERS_AS_SETTERS);
	      //MAPPER.disable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
	    }
	 
	    public MyObjectMapperProvider() {
	        System.out.println("Instantiate MyJacksonJsonProvider");
	    }
	     
	    @Override
	    public ObjectMapper getContext(Class<?> type) {
	        System.out.println("MyJacksonProvider.getContext() called with type: "+type);
	        return MAPPER;
	    } 
}
