package com.turddles.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import com.turddles.Utilities;
import com.turddles.models.Classroom;

public class ClassroomDaoImpl {

	public int getClassroomId(int tenantId, String classroomName){
		Integer id = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try {
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("SELECT classroom_id from classrooms WHERE name = ? AND tenant-id=?");
			stmt.setString(1, classroomName);
			stmt.setInt(2, tenantId);
			rs = stmt.executeQuery();
			
			while(rs.next()){
				id = rs.getInt("classroom_id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Utilities.closeConnection(rs, stmt, connect);	
		return id;
	}
	
	public List<Classroom> getAllClassrooms(int tenantId){
		Classroom classroom = null;
		List<Classroom> result = new ArrayList<Classroom>();
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("SELECT * FROM classrooms WHERE tenant_id=?");
			stmt.setInt(1, tenantId);
		
			rs = stmt.executeQuery();
		
			while(rs.next()){
				classroom = new Classroom(
						rs.getString("name"), 
						rs.getInt("start_age"),
						rs.getInt("end_age"), 
						rs.getInt("size"));
				result.add(classroom);
				}
		}
		catch(Exception e){
			System.err.println("Exception occurred " + e.getMessage());
		}
		// close connections
		Utilities.closeConnection(rs, stmt, connect);
		
		return result;
	}
	
	public Classroom getClassroom(int tenantId, String name) {
		Classroom classroom = null;
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("SELECT * FROM classrooms WHERE tenant_id=? AND name=?");
			stmt.setInt(1, tenantId);
			stmt.setString(2, name);
		
			rs = stmt.executeQuery();
		
			if(rs.next()){
				classroom = new Classroom(
						rs.getString("name"), 
						rs.getInt("start_age"),
						rs.getInt("end_age"), 
						rs.getInt("size"));
				}
		}
		catch(Exception e){
			
		}
		// close connections
		Utilities.closeConnection(rs, stmt, connect);
		
		return classroom;
	}
	
	public void addClassroom(int tenantId, Classroom classroom) throws Exception{
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try{
			if(getClassroom(tenantId, classroom.getName()) != null){
				throw new SQLIntegrityConstraintViolationException();
			}
			connect = Utilities.connectToDB();			
			stmt = connect.prepareStatement(
					"INSERT INTO classroom "
					+ "(tenant_id, name, start_age, end_age, size)"
					+ "VALUES (?,?,?,?,?)");
			stmt.setInt(1, tenantId);
			stmt.setString(2, classroom.getName());
			stmt.setInt(3, classroom.getStart_age());
			stmt.setInt(4, classroom.getSize());
			stmt.executeUpdate();
		}
		catch(Exception e){
			 throw e;
		}
	}
	
	public void updateClassroom(int tenantId, int classroom_id, Classroom classroom) throws Exception{
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement(
					"UPDATE classrooms SET name=?, start_age=?, end_age=?, size=? WHERE tenant_id=? & child_id=?");
			stmt.setString(1, classroom.getName());
			stmt.setInt(2, classroom.getStart_age());
			stmt.setInt(3, classroom.getEnd_age());
			stmt.setInt(4, classroom.getSize());
			stmt.executeUpdate();
			
		}catch (Exception e){
			throw e;
		}
		// close connections
		Utilities.closeConnection(rs, stmt, connect);
	}
	
	public void deleteClassroom(int tenantId, int classroomId){
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try {
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("DELETE FROM children WHERE tenant_id = AND classroom_id=?");
			stmt.setInt(1, tenantId);
			stmt.setInt(2, classroomId);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
}
