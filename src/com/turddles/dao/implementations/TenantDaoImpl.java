package com.turddles.dao.implementations;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.turddles.Utilities;
import com.turddles.models.Tenant;

public class TenantDaoImpl {

	public Integer getTenantId(String tenantName){
		Integer id = null;
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("SELECT id FROM tenants WHERE username=?");
			stmt.setString(1, tenantName);
			rs = stmt.executeQuery();
			
			if(rs.next()){
				id = rs.getInt("tenant_id");
			}
		}
		catch(Exception e){
			
		}
		return id;
	}
	
	public boolean authenticateUser(String username, String password) throws SQLException{
		Connection connect = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		connect = Utilities.connectToDB();
		stmt = connect.prepareStatement("SELECT * FROM tenants WHERE username=? AND password=?");
		stmt.setString(1, username);
		stmt.setString(2, password);
		rs = stmt.executeQuery();
		
		if(rs.next()){
			return true;
		}
		return false;
	}
	
	public boolean userExists(String username) throws SQLException{
		Connection connect = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		connect = Utilities.connectToDB();
		stmt = connect.prepareStatement("SELECT * FROM tenants WHERE username=?");
		stmt.setString(1, username);
		rs = stmt.executeQuery();
		
		if(rs.next()){
			return true;
		}
		return false;
	}
	
	public void saveToken(String username, String token) throws SQLException{
		Connection connect = null;
		PreparedStatement stmt = null;
		
		connect = Utilities.connectToDB();
		stmt = connect.prepareStatement("UPDATE tenants SET token=? WHERE username = ?");
		stmt.setString(1, token);
		stmt.setString(2, username);
		stmt.executeUpdate();
	}
	
	public void createTenant(Tenant tenant) throws SQLException{
		Connection connect = null;
		PreparedStatement stmt = null;
		
		connect = Utilities.connectToDB();
		stmt = connect.prepareStatement("INSERT INTO tenants (name, address, email, phone, status, username, password) "
				+ "VALUES (?,?,?,?,?,?,?)");
		stmt.setString(1, tenant.getTenantName());
		stmt.setString(2, tenant.getTenantAddress());
		stmt.setString(3, tenant.getTenantEmail());
		stmt.setString(4, tenant.getTenantPhone());
		stmt.setString(5, tenant.getTenantStatus());
		stmt.setString(6, tenant.getUsername());
		stmt.setString(7, tenant.getPassword());
		
		stmt.executeUpdate();
	}
	
	public boolean getToken(String username) throws SQLException{
		Connection connect = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		connect = Utilities.connectToDB();
		stmt = connect.prepareStatement("SELECT token FROM credentials WHERE username=?");
		stmt.setString(1, username);
		rs = stmt.executeQuery();
		
		if(rs.next()){
			return true;
		}
		return false;
	}
}
