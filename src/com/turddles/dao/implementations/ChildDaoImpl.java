package com.turddles.dao.implementations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import com.turddles.Utilities;
import com.turddles.dao.interfaces.ChildDao;
import com.turddles.models.Child;

public class ChildDaoImpl implements ChildDao{
	
	@Override
	public List<Child> getAllChildren(int tenantId){
		Child child = null;
		List<Child> result = new ArrayList<Child>();
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("SELECT * FROM children WHERE tenant_id=?");
			stmt.setInt(1, tenantId);
		
			rs = stmt.executeQuery();
		
			while(rs.next()){
				child = new Child(
						rs.getString("first_name"), 
						rs.getString("last_name"),
						rs.getDate("birthday"), 
						rs.getDate("enrollment_date"), 
						rs.getDate("departure_date"),
						rs.getString("enrollment_status"),
						rs.getString("weekly_schedule"),
						rs.getString("notes"),
						rs.getString("history"));
				result.add(child);
				}
		}
		catch(Exception e){
			System.err.println("Exception occurred " + e.getMessage());
		}
		// close connections
		Utilities.closeConnection(rs, stmt, connect);
		
		return result;
	}
	
	@Override
	public Child getChild(int tenantId, String firstName, String lastName) {
		Child child = null;
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("SELECT * FROM children WHERE tenant_id=? AND first_name=? AND last_name=?");
			stmt.setInt(1, tenantId);
			stmt.setString(2, firstName);
			stmt.setString(3, lastName);
		
			rs = stmt.executeQuery();
		
			if(rs.next()){
				child = new Child(
						rs.getString("first_name"), 
						rs.getString("last_name"), 
						rs.getDate("birthday"), 
						rs.getDate("enrollment_date"), 
						rs.getDate("departure_date"),
						rs.getString("enrollment_status"),
						rs.getString("weekly_schedule"),
						rs.getString("notes"),
						rs.getString("history"));
				
			//	child.setId(rs.getInt("child_id"));
				}
		}
		catch(Exception e){
			
		}
		// close connections
		Utilities.closeConnection(rs, stmt, connect);
		
		return child;
	}
	
	public void addChild(int tenantId, Child child, String classroomName) throws Exception{
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try{
			if(getChild(tenantId, child.getFirstName(), child.getLastName()) != null){
				throw new SQLIntegrityConstraintViolationException();
			}
			connect = Utilities.connectToDB();
			ClassroomDaoImpl classroom = new ClassroomDaoImpl();
			int classroomId = classroom.getClassroomId(tenantId, classroomName);
			
			stmt = connect.prepareStatement(
					"INSERT INTO children "
					+ "(tenant_id, classroom_id, first_name, last_name, birthday, weekly_schedule, enrollment_status, enrollment_date, departure_date, notes, history)"
					+ "VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setInt(1, tenantId);
			stmt.setInt(2, classroomId);
			stmt.setString(3, child.getFirstName());
			stmt.setString(4, child.getLastName());
			stmt.setDate(5, child.getBirthday());
			stmt.setString(6, child.getSchedule());
			stmt.setString(7, child.getEnrollmentStatus());
			stmt.setDate(8, child.getEnrollmentDate());
			stmt.setDate(9, child.getDepartureDate());
			stmt.setString(10, child.getNotes());
			stmt.setString(11, child.getHistory());
			stmt.executeUpdate();
		}
		catch(Exception e){
			 throw e;
		}
	}
	
	public void updateChild(int tenantId, int childId, Child child, String classroomName) throws Exception{
		ResultSet rs = null;
		Connection connect = null;
		PreparedStatement stmt = null;
		
		ClassroomDaoImpl classroom = new ClassroomDaoImpl();
		int classroomId = classroom.getClassroomId(tenantId, classroomName);
		
		try{
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement(
					"UPDATE children SET classroom_id=?, first_name=?, last_name=?, "
					+ "birthday=?, weekly_schedule=?, enrollment_status=?, enrollment_date=?, "
					+ "departure_date=?, notes=?, history=?"
					+ "WHERE tenant_id=? & child_id=?");
			stmt.setInt(1, classroomId);
			stmt.setString(2, child.getFirstName());
			stmt.setString(3, child.getLastName());
			stmt.setDate(4, child.getBirthday());
			stmt.setString(5, child.getSchedule());
			stmt.setString(6, child.getEnrollmentStatus());
			stmt.setDate(7, child.getEnrollmentDate());
			stmt.setDate(8, child.getDepartureDate());
			stmt.setString(9, child.getNotes());
			stmt.setString(10, child.getHistory());
			stmt.setInt(11, tenantId);
			stmt.setInt(12, childId);
			stmt.executeUpdate();
			
		}catch (Exception e){
			throw e;
		}
		// close connections
		Utilities.closeConnection(rs, stmt, connect);
	}
	
	public void deleteChild(int tenantId, int childId){
		Connection connect = null;
		PreparedStatement stmt = null;
		
		try {
			connect = Utilities.connectToDB();
			stmt = connect.prepareStatement("DELETE FROM children WHERE tenant_id = AND child_id=?");
			stmt.setInt(1, tenantId);
			stmt.setInt(2, childId);
			stmt.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
}
