package com.turddles.dao.interfaces;

import java.util.List;
import com.turddles.models.Child;;

public interface ChildDao {
	
//	void addChild(long tenantId, Child child, String classroomName);
	//void updateChild(long tenantId, Child child);
	//void deleteChild(long tenantId, Child child);
	List<Child> getAllChildren(int tenantId);
	Child getChild(int tenantId, String firstName, String lastName);
}
