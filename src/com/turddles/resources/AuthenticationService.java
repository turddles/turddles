package com.turddles.resources;

import java.security.Key;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.turddles.dao.implementations.TenantDaoImpl;
import com.turddles.models.Tenant;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

@Path("/login")
public class AuthenticationService {
	@POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser(Tenant tenant) {

        try {
            // Authenticate the user using the credentials provided
            if (authenticate(tenant.getUsername(), tenant.getPassword())){

            // Issue a token for the user
            String token = issueToken(tenant.getUsername());

            // Return the token on the response
            return Response.status(200).entity(token).build();
            }

        } catch (Exception e) {
            
        }      
        return Response.status(401).build();
    }
	
	private boolean authenticate(String username, String password) throws Exception{
		TenantDaoImpl dao = new TenantDaoImpl();
		
		try{
			
			return dao.authenticateUser(username, password);
			
		}catch (Exception e){
			throw e;
		}
	}
	
	private String issueToken(String username) throws SQLException{
		Key key = MacProvider.generateKey(); // get key from app config
		TenantDaoImpl dao = new TenantDaoImpl();
		
		String token = Jwts.builder().setSubject(username).signWith(SignatureAlgorithm.HS512, key).compact();
		dao.saveToken(username, token);
		return token;	
	}
}
