package com.turddles.resources;

import java.sql.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.turddles.Utilities;
import com.turddles.dao.implementations.CommunicationDaoImpl;
import com.turddles.models.Communication;

@Path("/communication")
public class CommunicationService {

	@Context SecurityContext securityContext; 
	private int tenantId = Utilities.getCurrentUserTenantId(securityContext);
	CommunicationDaoImpl dao = new CommunicationDaoImpl();
	
	@Path("/note/{date}")
	@GET
	public List<Communication> getNote(@PathParam("date") Date date ){
		return dao.getNote(tenantId, date, "note");
	}
	
	@Path("/alert/{date}")
	@GET
	public List<Communication> getAlert(@PathParam("date") Date date ){
		return dao.getNote(tenantId, date, "alert");
	}
	
	@Path("/note")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveNote(Communication communication){
		try{
			dao.saveNote(tenantId, communication, "note");
			return Response.status(200).entity(communication).build();
		}
		catch(Exception e){ // where to handle different exceptions?
			return Response.status(400).entity(e).build();
		}	
	}
	
	@Path("/alert")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveAlert(Communication communication){
		try{
			dao.saveNote(tenantId, communication, "alert");
			return Response.status(200).entity(communication).build();
		}
		catch(Exception e){ // where to handle different exceptions?
			return Response.status(400).entity(e).build();
		}	
	}
}
