package com.turddles.resources;

import java.sql.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.turddles.Utilities;
import com.turddles.dao.implementations.ChildDaoImpl;
import com.turddles.dao.implementations.TenantDaoImpl;
import com.turddles.models.Child;

@Path("/Children")
public class ChildService {

	@Context SecurityContext securityContext; 
	private int tenantId = Utilities.getCurrentUserTenantId(securityContext);
	private ChildDaoImpl dao = new ChildDaoImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Child> getChilden(){
		
		return dao.getAllChildren(tenantId);	  
	}
	
	@Path("{name}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Child getChild(@QueryParam("firstname") String firstname, @QueryParam("lastname") String lastname){
		return dao.getChild(tenantId, firstname, lastname);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addChild(@QueryParam("classroom") String classroom, Child child){
		try{
			dao.addChild(tenantId, child, classroom);
			return Response.status(200).entity(child).build();
		}
		catch(Exception e){ // where to handle different exceptions?
			return Response.status(400).entity(e).build();
		}		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response updateChild(@PathParam("id") int childId, @QueryParam("classroom")String classroom, Child child){
		try{
			dao.updateChild(tenantId, childId, child, classroom);
			return Response.status(200).entity(child).build();
		
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}		
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response deleteChild(@PathParam("id") int id){
		try{
			dao.deleteChild(tenantId, id);
			return Response.status(200).build();
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}
	} 
}
