package com.turddles.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.turddles.Utilities;
import com.turddles.dao.implementations.GuardianDaoImpl;
import com.turddles.models.Guardian;

public class GuardianService {

	@Context SecurityContext securityContext; 
	private int tenantId = Utilities.getCurrentUserTenantId(securityContext);
	private GuardianDaoImpl dao = new GuardianDaoImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Guardian> getAllGuardians(){
		
		return dao.getAllGuardians(tenantId);	  
	}
	
	@Path("{child}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Guardian> getChildGuardians(@QueryParam("firstname") String firstname, @QueryParam("lastname") String lastname){
		return dao.getChildGuardians(tenantId, firstname, lastname); // by childId??
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addGuardian(@QueryParam("childId") String childId, Guardian guardian){
		try{
			dao.addGuardian(tenantId, childId, guardian);
			return Response.status(200).entity(guardian).build();
		}
		catch(Exception e){ // where to handle different exceptions?
			return Response.status(400).entity(e).build();
		}		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response updateGuardian(@PathParam("id") int childId, Guardian guardian){
		try{
			dao.updateGuardian(tenantId, childId, guardian);
			return Response.status(200).entity(guardian).build();
		
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}		
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response deleteGuardian(@PathParam("id") int id){
		try{
			dao.deleteGuardian(tenantId, id);
			return Response.status(200).build();
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}
	} 
}
