package com.turddles.resources;

import java.sql.SQLIntegrityConstraintViolationException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.turddles.dao.implementations.TenantDaoImpl;
import com.turddles.models.Tenant;

@Path("/Register")
public class RegistrationService {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response register(Tenant tenant){
		TenantDaoImpl dao = new TenantDaoImpl();
		
		try{
			if(dao.userExists(tenant.getUsername())){
				throw new SQLIntegrityConstraintViolationException();
			}
			dao.createTenant(tenant);
			return Response.status(200).build();
			
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}	
	}
}
