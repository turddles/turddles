package com.turddles.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.turddles.Utilities;
import com.turddles.dao.implementations.ClassroomDaoImpl;
import com.turddles.models.Classroom;

@Path("/classroom")
public class ClassroomService {

	@Context SecurityContext securityContext; 
	private int tenantId = Utilities.getCurrentUserTenantId(securityContext);
	ClassroomDaoImpl dao = new ClassroomDaoImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Classroom> getClassrooms(){
		return dao.getAllClassrooms(tenantId);	  
	}
	
	@Path("{name}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Classroom getClassroom(@PathParam("classname") String classroomName){
		try{
			return dao.getClassroom(tenantId, classroomName);
		}catch(Exception e){ // where to handle different exceptions?
			return null;
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addClassroom(@QueryParam("classroom")Classroom classroom){
		try{
			dao.addClassroom(tenantId, classroom);
			return Response.status(200).entity(classroom).build();
		}
		catch(Exception e){ // where to handle different exceptions?
			return Response.status(400).entity(e).build();
		}		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response updateClassroom(@PathParam("id") int classroomId, @QueryParam("classroom")Classroom classroom){
		try{
			dao.updateClassroom(tenantId, classroomId, classroom);
			return Response.status(200).entity(classroom).build();
		
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}		
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response deleteClassroom(@PathParam("id") int id){
		try{
			dao.deleteClassroom(tenantId, id);
			return Response.status(200).build();
		}catch(Exception e){
			return Response.status(400).entity(e).build();
		}
	} 
}
